package com.example;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( convertir_a_dolares(3700, 350000) );
    }

    //Convertir de dolares a pesos
    public static double convertir_a_pesos(double valor_dolar, double cant_dolares){
        double resultado = valor_dolar * cant_dolares;
        return resultado;
    }

    //Convertir de pesos a dolares
    public static double convertir_a_dolares(double valor_dolar, double cant_pesos){
        double resultado = cant_pesos/valor_dolar;
        return resultado;
    }

    public static String pedir_fecha(){  
        Scanner sc = new Scanner(System.in);
        System.out.println("Por favor digite su fecha de nacimiento en números, separada por / : ");
        String fecha = sc.nextLine();
        sc.close();
        return (String) fecha;
    }

    public static void calcular_suerte(String fecha){  
        String[] arreglo_fecha = fecha.split("/");
        int suma = Integer.parseInt(arreglo_fecha[0])+Integer.parseInt(arreglo_fecha[1])+Integer.parseInt(arreglo_fecha[2]);
        System.out.println(suma);
    }
}